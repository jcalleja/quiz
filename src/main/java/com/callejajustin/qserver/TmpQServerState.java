package com.callejajustin.qserver;

import java.io.IOException;
import java.io.InputStream;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.callejajustin.qserver.json.Questions;

public class TmpQServerState {

	private Questions questions;

	public void initState() {
		ObjectMapper mapper = new ObjectMapper();
//		printClasspath();
		try {
			InputStream is = this.getClass().getClassLoader().getResourceAsStream("json/questions.json");
			this.questions = mapper.readValue(is, Questions.class);
			
			System.out.println("----------- Questions -----------");
			System.out.println(questions);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

//	private void printClasspath() {
//		System.out.println("-------- Start of print classpath --------");
//		
//		final java.lang.String list = java.lang.System
//				.getProperty("java.class.path");
//		System.out.println("java.class.path = " + list);
//		System.out.println("-------- End of print classpath --------");
//	}

}
