package com.callejajustin.qserver.json;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Question {

	private int id;
	private String question;
	private List<String> possibleAnswers;
	private List<Integer> correctAnswersIndex;
	private String questionType;

	public String getQuestion() {
		return question;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<String> getPossibleAnswers() {
		return possibleAnswers;
	}

	public void setPossibleAnswers(List<String> possibleAnswers) {
		this.possibleAnswers = possibleAnswers;
	}

	public List<Integer> getCorrectAnswersIndex() {
		return correctAnswersIndex;
	}

	public void setCorrectAnswersIndex(List<Integer> correctAnswersIndex) {
		this.correctAnswersIndex = correctAnswersIndex;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}

}
