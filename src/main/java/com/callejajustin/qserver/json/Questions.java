package com.callejajustin.qserver.json;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Questions {
	
	private List<Question> questions;

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}

}
